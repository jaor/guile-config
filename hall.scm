(hall-description
  (name "config")
  (prefix "guile")
  (version "0.5.0")
  (author "Alex Sassmannshausen")
  (copyright (2016 2017 2018 2020))
  (synopsis
    "Guile application configuration parsing library.")
  (description
    "Guile Config is a library providing a declarative approach to application configuration specification.  The library provides clean configuration declaration forms, and processors that take care of: configuration file creation; configuration file parsing; command-line parameter parsing using getopt-long; basic GNU command-line parameter generation (--help, --usage, --version); automatic output generation for the above command-line parameters.")
  (home-page
    "https://gitlab.com/a-sassmannshausen/guile-config")
  (license gpl3+)
  (dependencies `())
  (files (libraries
           ((scheme-file "config")
            (directory
              "config"
              ((directory "parser" ((scheme-file "sexp")))
               (scheme-file "getopt-long")
               (scheme-file "records")
               (scheme-file "api")
               (scheme-file "licenses")
               (scheme-file "helpers")))))
         (tests ((directory
                   "tests"
                   ((scheme-file "api")
                    (scheme-file "getopt-long")
                    (scheme-file "quickcheck")
                    (scheme-file "quickcheck-defs")
                    (scheme-file "config")))))
         (programs ((directory "scripts" ())))
         (documentation
           ((symlink "README" "README.org")
            (org-file "README")
            (text-file "HACKING")
            (text-file "COPYING")
            (text-file "NEWS")
            (text-file "ChangeLog")
            (text-file "AUTHORS")
            (directory "doc" ((texi-file "config")))
            (directory
              "examples"
              ((scheme-file "simple")
               (scheme-file "frobnigator")
               (scheme-file "hello-world")))))
         (infrastructure
           ((scheme-file "guix")
            (scheme-file "hall")
            (text-file ".gitignore")))))
